$("#sendMail").on("click", function(){
    var name=$("#name").val().trim();
    var email=$("#email").val().trim();
    var phone=$("#phone").val().trim();
    var message=$("#message").val();

    if(name=="")
{
$("#errorMess").text("Введите имя");
    return false;
}    
else if(email=="")
{
$("#errorMess").text("Введите email");
    return false;
}
else if(phone=="")
{
$("#errorMess").text("Введите номер телефона");
    return false;
}
else if(message=="")
{
$("#errorMess").text("Введите текст сообщения");
    return false;
}

    $("#errorMess").text("");

    $.ajax( {
        url:'feedback/mail.php',
        type: 'POST',
        cache: false,
        data: {'name': name, 'email': email, 'phone': phone, 'message': message},
        dataType: 'html',
        beforeSend: function()
        {
            $("#sendMail").prop("disabled",true);
        },
        success: function(data){
            if (data == false)
             alert ("Были ошибки сообщение не отправлено");
             else
             $("#mailForm").trigger("reset");
            $("#sendMail").prop("disabled",false);
        }
                } );
     
} );